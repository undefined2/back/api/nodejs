import { RegisterParams } from './auth.params'
import { ParamsError } from '../../infrastructure/services/rpc/errors'
import { MethodResult } from '../../infrastructure/services/rpc/results'
import { UserModel } from '../../db/models/user.model'
import { NeedAuth } from '../../infrastructure/services/auth/auth.decorators'

export class AuthService {
    @NeedAuth()
    async register(credentials) {
        const registerParams = await RegisterParams.build(credentials)
        if (registerParams.validationErrors.length > 0) {
            return new ParamsError(registerParams.validationErrors)
        }
        const userModel = await UserModel.findOne({where: {email: 'email'}})
        return new MethodResult({
            accessToken: registerParams.email,
            refreshToken: registerParams.password
        })
    }
    signIn(credential) {
        return {
            accessToken: credential.email,
            refreshToken: credential.password
        }
    }
}
