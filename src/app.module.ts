import { CacheModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { RouterService } from './infrastructure/services/router/router.service';
import { AuthService } from './domain/auth/auth.service';
import { RpcService } from './infrastructure/services/rpc/rpc.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { ConfigModule } from '@nestjs/config';
import { ORMModels } from './db/models'

if (!Array.isArray((global as any).authenticatedMethods)) {
	(global as any).authenticatedMethods = [];
}

@Module({
	imports: [
		CacheModule.register(),
		ConfigModule.forRoot({
			envFilePath: ['.env.database'],
		}),
		SequelizeModule.forFeature([...ORMModels]),
		SequelizeModule.forRoot({
			dialect: 'postgres',
			host: '0.0.0.0',
			port: 5432,
			username: 'ndfnd',
			password: 'ndfnd',
			database: 'ndfnd',
			autoLoadModels: true,
			synchronize: true,
		}),
	],
	exports: [RpcService],
	controllers: [AppController],
	providers: [RouterService, AuthService, RpcService],
})
export class AppModule {}
