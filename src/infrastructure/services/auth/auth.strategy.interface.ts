export interface AuthStrategyInterface {
    checkAuth(): boolean;
}