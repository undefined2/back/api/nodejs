import { RpcError } from '../rpc/errors'

export class AuthError extends RpcError {
	constructor(data = undefined) {
		super(100, 'Auth error', data);
	}
}
