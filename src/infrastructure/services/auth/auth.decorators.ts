export function NeedAuth(needAuth = true) {
    return function(
      target: any,
      propertyKey: string,
      descriptor: PropertyDescriptor
    ) {
        if (!Array.isArray((global as any).authenticatedMethods)) {
            (global as any).authenticatedMethods = [];
        }
        if (needAuth === true) {
            (global as any).authenticatedMethods.push({
                className: target.constructor.name,
                methodName: propertyKey
            })
        }
    };
}
