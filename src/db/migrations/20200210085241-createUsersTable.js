'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.sequelize.query(`
			start transaction;
			
			    create table if not exists users (
			       id serial CONSTRAINT id_pk PRIMARY KEY,
			       email varchar(128) NOT NULL,
			       "emailComfirmed" boolean default false,
			       password varchar(128) not null,
			       updated timestamp with time zone default current_timestamp,
			       created timestamp with time zone default current_timestamp
			    );
			
			    create or replace function "updateUsersTrigger"()
			        returns trigger as $$
			        begin
			            new.updated = now();
			            return new;
			        end;
			        $$ language 'plpgsql';
			
			    CREATE TRIGGER "updateUsersTrigger"
			        BEFORE UPDATE ON users
			        FOR EACH ROW
			        EXECUTE PROCEDURE "updateUsersTrigger"();
			
			    comment on column users.id is 'User unique autoincremented identificator';
			    comment on column users.email is 'User email (used as login)';
			    comment on column users."emailComfirmed" is 'Is user email confirmed';
			    comment on column users.password is 'User password';
			    comment on column users.updated is 'Last time user was updated';
			    comment on column users.created is 'Time user was created';
			
			commit;
        `);
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.sequelize.query(`
			start transaction;
				drop table if exists users;
				drop function if exists "updateUsersTrigger";
			commit;
		`);
	}
};
