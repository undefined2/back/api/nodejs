import { UserModel } from './user.model'

const ORMModels = [
  UserModel
]

export {
  ORMModels
}