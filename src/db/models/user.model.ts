import { Column, UpdatedAt, CreatedAt, Model, Table } from 'sequelize-typescript';

@Table({
  tableName: 'users'
})
export class UserModel extends Model<UserModel> {
  @Column
  email: string

  @Column
  password: string

  @UpdatedAt
  updated: Date

  @CreatedAt
  created: Date
}
